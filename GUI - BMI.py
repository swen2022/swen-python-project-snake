from tkinter import *
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from PIL import Image,ImageTk

root=Tk()
root.title("SWEN HS 2023 - BMI Rechner")
root.geometry("600x500+300+200")
root.resizable(False,False) #lässt keine Änderungen an den Proportionen des Fensters zu
root.configure(bg="#EDEDED") #Hintergrundfarbe (bg) 

#Logo oben links (klein)
image_logo=PhotoImage(file="Images/Logo.png")
root.iconphoto(False,image_logo)

#Titel
Titel = Text(root, height=1, width=80)
Titel.pack()
Titel.insert(END, "BMI Rechner")
Titel.configure(bg="#8496B0", font='Arial 18 bold', fg='white')
Titel.tag_configure("center", justify='center') #wird benötigt um den Titel zu zentrieren
Titel.tag_add("center",1.0, 'end') #wird benötigt um den Titel zu zentrieren

#Bild auf der rechten Seite
Bild_Waage=PhotoImage(file="Images/BMI.png")
Waage=Label(root, image=Bild_Waage)
Waage.place(x=380, y=65)

#Unterer Teil
Label(root,width=90, height=15, bg="#FFFAFA").pack(side=BOTTOM)

#Felder Beschreibung
Feld_Gewicht = Text(root, height=1, width=15)
Feld_Gewicht.pack()
Feld_Gewicht.insert(END, "Gewicht in kg:")
Feld_Gewicht.configure(bg="#DBDBDB", font='Arial 11')
Feld_Gewicht.tag_configure("center", justify='center') 
Feld_Gewicht.tag_add("center",1.0, 'end')
Feld_Gewicht.place(x=20, y=80)

Feld_Grösse = Text(root, height=1, width=15)
Feld_Grösse.pack()
Feld_Grösse.insert(END, "Grösse in m:")
Feld_Grösse.configure(bg="#DBDBDB", font='Arial 11')
Feld_Grösse.tag_configure("center", justify='center') 
Feld_Grösse.tag_add("center",1.0, 'end')
Feld_Grösse.place(x=20, y=120)

#Eingabefelder (kg)
Eingabefeld_Gewicht = Entry(bg="white")
Eingabefeld_Gewicht.place (x=200, y=80, width=100, height=20)

#Eingabefelder (m)
Eingabefeld_Grösse = Entry(bg="white")
Eingabefeld_Grösse.place (x=200, y=120, width=100, height=20)

label1=Label(root, bg="#FFFAFA", height=9, width=45, font="arial 14")
label1.place(x=110, y=270)

Bild_Ampel_rot=PhotoImage(file="Images/Ampel_rot.png")
Bild_Ampel_grün=PhotoImage(file="Images/Ampel_grün.png")
Bild_Ampel_orange=PhotoImage(file="Images/Ampel_orange.png")
BildAmpel=Label(root)
BildAmpel.place(x=45, y=300)

# Berechnung BMI 
def Berechnung_BMI():
    #Übernahme der Daten 
    try:
        Gewicht = float(Eingabefeld_Gewicht.get())
        Grösse = float(Eingabefeld_Grösse.get())
    except ValueError:
        messagebox.showerror ("Fehler", "Bitte gültige Werte für Gewicht (in kg) und Grösse (in m) eingeben")
        return
    #Berechnung 
    BMI = Gewicht/(Grösse*Grösse)
    AnzeigeBMI = f"{BMI:0.1f}"
    #Daten Anzeigen
    Wert_BMI.config(text=AnzeigeBMI) #ACHTUNG RECHTSCHREIBUNG, wir haben Text statt text verwendet

    if BMI<18.5:
        label1.config(text="Underweight!" "\n" "Ihr BMI deutet auf Untergewicht hin.")
    elif BMI >18.5 and BMI<=25:
        label1.config(text="Normal!" "\n" "Ihr BMI liegt im Normalbereich.")
    elif BMI>25 and BMI<=30:
        label1.config(text="Overweight!" "\n" "Ihr BMI deutet auf Übergewicht hin.")
    else: 
        label1.config(text="Massive Overweight!" "\n" "Ihr BMI deutet auf starkes Übergewicht hin.")
        
    if BMI<18.5:
        BildAmpel.config(image = Bild_Ampel_rot)
    elif BMI >18.5 and BMI<=25:
        BildAmpel.config(image = Bild_Ampel_grün)
    elif BMI>25 and BMI<=30:
        BildAmpel.config(image = Bild_Ampel_orange)
    else: 
        BildAmpel.config(image = Bild_Ampel_rot)
        
Button_Berechnen = Button(root, height=1, width=30, text='BMI berechnen', command=Berechnung_BMI)
Button_Berechnen.pack()
Button_Berechnen.configure(bg='#CAE1FF', font='Arial 11 bold')
Button_Berechnen.place(x=20, y=160)

#Feld Beschreibung
Feld_BMIWert = Text(root, height=1, width=15)
Feld_BMIWert.pack()
Feld_BMIWert.insert(END, "BMI Wert:")
Feld_BMIWert.configure(bg="#333333", font='Arial 11 bold', fg='white')
Feld_BMIWert.tag_configure("center", justify='center') 
Feld_BMIWert.tag_add("center",1.0, 'end')
Feld_BMIWert.place(x=20, y=220)

Wert_BMI = Label(root, bg='white', text='')
Wert_BMI.place(x=200, y=220, width=100, height=20)

mainloop()
root.mainloop()

#fertiger Code