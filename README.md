

# BMI-Rechner

Die vorliegende GUI-Applikation stellt ein BMI-Rechner dar. Der User kann Grösse und Gewicht selbständig eingeben und den Button "berechnen" klicken. Dadurch wird anhand der eingegebenen Daten der BMI berechnet. Ebenfalls wird abhängig vom BMI-Ergebnis eine entsprechende Interpretation in Form von Bild und Text angezeigt. 

## Get started

Der GUI wird in der Datei "GUI-BMI.py" gestartet. Durch die Funktion "Run Python File" wird der GUI gestartet und der User kann seine Werte eingeben.

Bitte vorgängig installieren: pillow

``
    python main.py
``
``
    pip install -r requirements.txt
``

## Understanding the sources

Der BMI wird wie folgt berechnet:
Körpergewicht in kg / (Körpergrösse in Meter)2

Verwendete Skala:
#<18.5 = Underweight (Ampel rot)
#>18.5 und <=25 = Normal (Ampel grün)
#>25 und <=30 = Overweight (Ampel orange)
#>30 = Massive Overweight (Ampel rot)

Die Skala orientiert sich lediglich an Richtwerten und gibt nicht in jedem Fall eine korrekte Aussage zu Über- oder Untergewicht wieder.
